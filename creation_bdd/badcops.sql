CREATE TABLE "public.id_bnvd_substance" (
	"substance" TEXT NOT NULL,
	"cas" TEXT NOT NULL,
	CONSTRAINT "id_bnvd_substance_pk" PRIMARY KEY ("substance","cas")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.id_sandre_x_id_bnvd" (
	"CdParametre" integer NOT NULL,
	"NomParametre" TEXT NOT NULL,
	"substance" TEXT NOT NULL,
	"cas" TEXT NOT NULL,
	CONSTRAINT "id_sandre_x_id_bnvd_pk" PRIMARY KEY ("CdParametre")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.infos_sandre" (
	"CdParametre" integer NOT NULL,
	"NomParametre" TEXT NOT NULL,
	"cas" TEXT NOT NULL,
	"CdGroupeParametres" TEXT,
	"NomGroupeParametres" TEXT,
	"famille" TEXT,
	"foonction" TEXT,
	"substance" TEXT NOT NULL
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.id_eu_pdb_x_id_bnvd" (
	"substance_id_eupbd" integer NOT NULL,
	"substance_eupbd" TEXT NOT NULL,
	"substance_bnvd" TEXT NOT NULL,
	"cas_bnvd" TEXT NOT NULL,
	CONSTRAINT "id_eu_pdb_x_id_bnvd_pk" PRIMARY KEY ("substance_id_eupbd","substance_eupbd")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "public.infos_eu_pdb" (
	"substance_id_eupbd" integer NOT NULL,
	"substance_eupbd" TEXT NOT NULL,
	"status_reg_1107_2019" TEXT,
	"date_approbation" DATE,
	"date_expiration_approbation" DATE,
	"candidat_substitution" TEXT,
	"substance_basique" TEXT,
	"risque_bas" TEXT,
	"autorisations_pays" TEXT,
	"autorisation_france" TEXT,
	"substance_bnvd" TEXT NOT NULL,
	"cas_bnvd" TEXT NOT NULL
) WITH (
  OIDS=FALSE
);




ALTER TABLE "id_sandre_x_id_bnvd" ADD CONSTRAINT "id_sandre_x_id_bnvd_fk0" FOREIGN KEY ("substance") REFERENCES "id_bnvd_substance"("substance");
ALTER TABLE "id_sandre_x_id_bnvd" ADD CONSTRAINT "id_sandre_x_id_bnvd_fk1" FOREIGN KEY ("cas") REFERENCES "id_bnvd_substance"("cas");

ALTER TABLE "infos_sandre" ADD CONSTRAINT "infos_sandre_fk0" FOREIGN KEY ("CdParametre") REFERENCES "id_sandre_x_id_bnvd"("CdParametre");
ALTER TABLE "infos_sandre" ADD CONSTRAINT "infos_sandre_fk1" FOREIGN KEY ("NomParametre") REFERENCES "id_sandre_x_id_bnvd"("NomParametre");
ALTER TABLE "infos_sandre" ADD CONSTRAINT "infos_sandre_fk2" FOREIGN KEY ("cas") REFERENCES "id_bnvd_substance"("substance");

ALTER TABLE "id_eu_pdb_x_id_bnvd" ADD CONSTRAINT "id_eu_pdb_x_id_bnvd_fk0" FOREIGN KEY ("substance_bnvd") REFERENCES "id_bnvd_substance"("substance");
ALTER TABLE "id_eu_pdb_x_id_bnvd" ADD CONSTRAINT "id_eu_pdb_x_id_bnvd_fk1" FOREIGN KEY ("cas_bnvd") REFERENCES "id_bnvd_substance"("cas");

ALTER TABLE "infos_eu_pdb" ADD CONSTRAINT "infos_eu_pdb_fk0" FOREIGN KEY ("substance_id_eupbd") REFERENCES "id_eu_pdb_x_id_bnvd"("substance_id_eupbd");
ALTER TABLE "infos_eu_pdb" ADD CONSTRAINT "infos_eu_pdb_fk1" FOREIGN KEY ("substance_eupbd") REFERENCES "id_eu_pdb_x_id_bnvd"("substance_eupbd");
ALTER TABLE "infos_eu_pdb" ADD CONSTRAINT "infos_eu_pdb_fk2" FOREIGN KEY ("substance_bnvd") REFERENCES "id_bnvd_substance"("substance");
ALTER TABLE "infos_eu_pdb" ADD CONSTRAINT "infos_eu_pdb_fk3" FOREIGN KEY ("cas_bnvd") REFERENCES "id_bnvd_substance"("cas");






